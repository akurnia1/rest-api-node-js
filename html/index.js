const getUser = () => {
  // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
  fetch("http://localhost:3000/posts")
    .then((response) => response.json())
    .then((resJson) => {
      const data = resJson.data;
      let ulPost = document.getElementById("posts");
      let content = "";
      data.forEach((d) => {
        content += `<li> id: ${d.id} <br> title ${d.title} <br> body ${d.body}</li>`;
      });
      ulPost.innerHTML = content;
    })
    .catch((err) => console.log("err", err));
  //   axios

  //   console.log("getUser");
};

getUser();

function addPostNew() {
  const title = document.getElementById("title").value;
  const body = document.getElementById("body").value;
  //   alert("test");
  axios
    .post("http://localhost:3000/posts", {
      title,
      body,
    })
    .then((res) => {
      alert("berhasil menambahkan data");
    })
    .catch((err) => {
      console.log("err", err);
    });
}
