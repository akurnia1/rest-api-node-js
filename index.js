const express = require("express");
var cors = require("cors");
const app = express();

app.use(cors());

const dataPosts = require("./db/posts.json");
const PATH_POSTS = "./db/posts.json";
const fs = require("fs");

var bodyParser = require("body-parser");
const { json } = require("body-parser");

// parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

function isNumber(n) {
  return /^-?[\d.]+(?:e-?\d+)?$/.test(n);
}

app.get("/posts", function (req, res) {
  fs.readFile(PATH_POSTS, "utf-8", (err, posts) => {
    if (err) {
      res.status(400).json({
        message: "Api in problem",
      });
    } else {
      res.status(200).json({
        data: JSON.parse(posts),
      });
    }
  });
});

// ini pake params
app.get("/posts/:id", (req, res) => {
  const id = req.params.id;
  console.log("id", id);
  if (isNumber(id)) {
    fs.readFile(PATH_POSTS, "utf-8", (err, data) => {
      const posts = JSON.parse(data);
      const post = posts.filter((d) => d.id == id);
      if (post.length) {
        // kalo post nya lebih dari 0 == true
        res.json({
          message: "Get by query id",
          post: post[0],
          // post: post
        });
      } else {
        res.status(404).json({
          message: "id not found",
          // post: post
        });
      }
    });
  } else {
    res.status(400).json({
      message: "Please input id with number",
    });
  }
});

app.post("/posts", (req, res) => {
  const { body, title } = req.body;
  // ini sama kek dibawah
  //   const body = req.body.body;
  //   const title = req.body.title;
  //   body = null, body = undefined, body = ""
  if (!body || !title) {
    res.status(400).json({
      message: "Please fulfill all required input",
    });
  } else {
    fs.readFile(PATH_POSTS, "utf-8", (err, data) => {
      let posts = JSON.parse(data);
      const objToAdd = {
        id: posts[posts.length - 1].id + 1,
        title,
        body,
      };
      posts.push(objToAdd);
      fs.writeFile(PATH_POSTS, JSON.stringify(posts), (err) => {
        res.status(201).json({
          message: "Succesfully create data",
          data: objToAdd,
        });
      });
    });
  }
});

app.put("/posts/:id", (req, res) => {
  let posts = dataPosts;
  let id = req.params.id;
  let { body, title } = req.body;
  let idFound = false;
  posts.forEach((p) => {
    if (p.id == id) {
      p.title = title;
      p.body = body;
      idFound = id;
    }
  });
  if (idFound) {
    fs.writeFile(PATH_POSTS, JSON.stringify(posts), (err) => {
      res.status(200).json({
        message: "Succesfully updated data",
        data: {
          body,
          title,
          id: idFound,
        },
      });
    });
  } else {
    res.status(404).json({
      message: "Data not found in our database",
    });
  }
});

app.delete("/posts/:id", (req, res) => {
  const posts = dataPosts;
  const id = req.params.id;
  let filterData = posts.filter((p) => p.id != id);
  const rowsChanged = posts.length - filterData.length;
  fs.writeFile(PATH_POSTS, JSON.stringify(filterData), (err) => {
    res.json({
      message: "Successfully deleted data",
      rowsChanged,
    });
  });
});

// // ini pake query
// app.get("/posts/getDetail/queryID", (req, res) => {
//   const id = req.query.id;
//   console.log("id", id);
//   if (isNumber(id)) {
//     fs.readFile(PATH_POSTS, "utf-8", (err, data) => {
//       const posts = JSON.parse(data);
//       const post = posts.filter((d) => d.id == id);
//       if (post.length) {
//         // kalo post nya lebih dari 0 == true
//         res.json({
//           message: "Get by id",
//           post: post[0],
//           // post: post
//         });
//       } else {
//         res.status(404).json({
//           message: "id not found",
//           // post: post
//         });
//       }
//     });
//   } else {
//     res.status(400).json({
//       message: "Please input id with number",
//     });
//   }
// });

app.listen(3000, () => {
  console.log("listening on port 3000");
});
